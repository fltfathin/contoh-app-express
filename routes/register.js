// step 3: copy dari index.js
var express = require('express');
var router = express.Router();

/* GET home page. */

router.get('/', function(req, res, next) {
  res.render('register', { title: 'Express' });
});

// step 6: lakukan hal yang sama untuk post request
router.post('/', function(req, res, next) {
  console.log(req.body);
  res.render('registerd', { title: 'Express' , nama: req.body.nama, alamat: req.body.alamat, email: req.body.email});
});

module.exports = router;
